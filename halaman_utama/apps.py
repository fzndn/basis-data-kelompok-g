from django.apps import AppConfig


class HalamanUtamaConfig(AppConfig):
    name = 'halaman_utama'
