from django import forms

from event_cud.database import kategori_choice


class OrganisasiForm(forms.Form):
    nama = forms.CharField(
        label='Nama Organisasi',
        max_length=50,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control'
            }
        ))

    email = forms.CharField(
        label='Email Organisasi',
        max_length=50,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control'
            }
        ))

    alamat_website = forms.CharField(
        label='Web Organisasi',
        max_length=50,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control'
            }
        ))

    tingkatan =  forms.ChoiceField(
        label='Tingkatan Organisasi',
        choices=[
            ('fakultas', 'fakultas'),
            ('universitas', 'universitas'),
        ],
        widget=forms.Select(
            attrs={
                'class': 'form-control',
            }
        ))

    kategori = forms.ChoiceField(
        label='Kategori Organisasi',
        choices=kategori_choice(),
        widget=forms.Select(
            attrs={
                'class': 'form-control'
            }
        ))

    deskripsi = forms.CharField(
        label='Deskripsi',
        widget=forms.TextInput(
            attrs={
                'class': 'form-control'
            }
        ))

    contact_person = forms.CharField(
        label='Contact Person',
        max_length=20,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control'
            }
        ))
