from datetime import datetime

from django.http import HttpResponse
from django.shortcuts import render, redirect

from event_cud.database import insert_event, get_events, delete_event, get_one_event, \
    get_pembuat_event_name
from event_cud.forms import EventForm


def event_index(request):
    events = get_events();
    return render(request, 'event_index.html', {'events': events})


def event_delete(request, id_event, id_pembuat_event):
    delete_event(id_event, id_pembuat_event)
    return redirect('event-create')


def event_update(request, id_event, id_pembuat_event):
    print("masuk update")
    print(id_event)
    print(id_pembuat_event)
    event = get_one_event(id_event, id_pembuat_event)
    print(event)
    form = EventForm(
        initial={'nama': event[0],
                 'tanggal': event[1],
                 'waktu': event[2],
                 'kapasitas': event[3],
                 'harga_tiket': event[4],
                 'lokasi': event[5],
                 'sifat': (event[6], event[6]),
                 'deskripsi': event[7],
                 'kategori': event[8],
                 'pembuat_event': (event[9], get_pembuat_event_name(event[9]))
                 }
    )
    response = render(request, 'event_update.html', {'form': form})
    response.set_cookie('id_event',id_event)
    return response


def event_update_post(request):
    if request.method == 'POST':
        form = EventForm(request.POST)
        if form.is_valid():
            nama = request.POST['nama']
            tanggal = request.POST['tanggal']
            waktu = request.POST['waktu']
            kapasitas = request.POST['kapasitas']
            harga_tiket = request.POST['harga_tiket']
            lokasi = request.POST['lokasi']
            sifat = request.POST['sifat']
            kategori = request.POST['kategori']
            deskripsi = request.POST['deskripsi']
            id_pembuat_event = request.POST['pembuat_event']
            d = datetime.strptime(tanggal, '%Y-%m-%d')
            tanggal = d.strftime('%Y/%m/%d')
            waktu = waktu
            print(tanggal)
            print(waktu)
            delete_event(request.COOKIES["id_event"], id_pembuat_event)
            insert_event(nama=nama,
                         id_pembuat_event=id_pembuat_event,
                         tanggal=tanggal,
                         waktu=waktu,
                         kapasitas=kapasitas,
                         harga_tiket=harga_tiket,
                         lokasi=lokasi,
                         sifat_event=sifat,
                         kategori=kategori,
                         deskripsi_singkat=deskripsi)
            return redirect('event')
        else:
            return HttpResponse("form is not valid")
    return redirect('event')

def event_reg(request):
    if request.method == 'POST':
        form = EventForm(request.POST)
        if form.is_valid():
            nama = request.POST['nama']
            tanggal = request.POST['tanggal']
            waktu = request.POST['waktu']
            kapasitas = request.POST['kapasitas']
            harga_tiket = request.POST['harga_tiket']
            lokasi = request.POST['lokasi']
            sifat = request.POST['sifat']
            kategori = request.POST['kategori']
            deskripsi = request.POST['deskripsi']
            id_pembuat_event = request.POST['pembuat_event']
            d = datetime.strptime(tanggal, '%Y-%m-%d')
            tanggal = d.strftime('%Y/%m/%d')
            waktu = waktu
            print(tanggal)
            print(waktu)
            insert_event(nama=nama,
                         id_pembuat_event=id_pembuat_event,
                         tanggal=tanggal,
                         waktu=waktu,
                         kapasitas=kapasitas,
                         harga_tiket=harga_tiket,
                         lokasi=lokasi,
                         sifat_event=sifat,
                         kategori=kategori,
                         deskripsi_singkat=deskripsi)
            return redirect('event')
        else:
            return HttpResponse("form is not valid")

    form = EventForm()
    events = get_events();
    return render(request, 'event_create.html', {'form': form, 'events': events})


def event_detail(request):
    return render(request, 'daftar_event.html')
