import psycopg2
from simui.settings import DATABASE_CONFIG

def cekLogin(username):
    conn = None
    try:
        params = DATABASE_CONFIG
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        cur.execute("SELECT * FROM pengguna where username=%s", (username,))
        row = cur.fetchone()
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
    return row

def cekRole(username):
    conn = None
    try:
        params = DATABASE_CONFIG
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        cur.execute("SELECT * FROM admin where username=%s", (username,))
        row = cur.fetchone()
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
        if row is not None:
            return "admin"
    return "nonadmin"