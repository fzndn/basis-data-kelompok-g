import psycopg2
from simui.settings import DATABASE_CONFIG

    # update pembuat_event set id='01' where id='1';
    # update pembuat_event set id='02' where id='2';
    # update pembuat_event set id='03' where id='3';
    # update pembuat_event set id='04' where id='4';
    # update pembuat_event set id='05' where id='5';
    # update pembuat_event set id='06' where id='6';
    # update pembuat_event set id='07' where id='7';
    # update pembuat_event set id='08' where id='8';
    # update pembuat_event set id='09' where id='9';

def kategori_choice():
    choices = []
    conn = None
    try:
        params = DATABASE_CONFIG
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        cur.execute("SELECT nomor, nama FROM kategori_event ORDER BY nomor")
        row = cur.fetchone()

        while row is not None:
            # print(row)
            choices.append((int(row[0]), row[1]))
            row = cur.fetchone()

        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
    return choices

def delete_organisasi(id_organisasi):
    organisasi = []
    conn = None
    try:
        params = DATABASE_CONFIG
        conn = psycopg2.connect(**params)
        cur = conn.cursor()

        cur.execute("""
        DELETE FROM organisasi
        WHERE id_organisasi=%s""",
        (id_organisasi),)

        rows_deleted = cur.rowcount
        print("rows deleted = " + str(rows_deleted))
        conn.commit()

        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
    return organisasi


def get_organisasi():
    organisasi = []
    conn = None
    try:
        # SELECT nama, tanggal, waktu, kapasitas, harga_tiket, lokasi, sifat_event, deskripsi_singkat, nomor_kategori, jumlah_pendaftar
        params = DATABASE_CONFIG
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        cur.execute("""
        SELECT nama
        FROM organisasi, pembuat_event
        where id = id_organisasi
        order by nama
        """)
        row = cur.fetchone()

        while row is not None:
            # print(row)
            organisasi.append(row)
            row = cur.fetchone()

        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
    return organisasi

def insert_organisasi(nama,
                 email,
                 alamat_website,
                 tingkatan,
                 kategori,
                 deskripsi,
                 contact_person):

    """ insert a new organization into the vendors table """
    sql = """
    INSERT INTO pembuat_event
    (id, nama, email, alamat_website, tingkatan, kategori, logo, 
    deskripsi, contact_person) 
    VALUES (%s,%s,%s,%s,%s,%s,NULL,%s,%s)
    """
    sql2 = """
    INSERT INTO organisasi
    (id_organisasi) 
    VALUES (%s)
    """
    conn = None
    organisasi_id = None
    try:
        print("masuk database")
        # read database configuration
        params = DATABASE_CONFIG
        # connect to the PostgreSQL database
        conn = psycopg2.connect(**params)
        # create a new cursor
        cur = conn.cursor()
        cur.execute('''
        SELECT id from pembuat_event order by id desc
        LIMIT 1
        ''')
        id_now = int(cur.fetchone()[0]) + 1

        id_str = str(id_now)

        cur.close()
        cur = conn.cursor()
        cur.execute(sql, (id_str, nama, email, alamat_website, tingkatan, kategori, deskripsi, contact_person))

        cur.execute(sql2, (id_str,))

        #event_id = cur.fetchone()[0]

        conn.commit()
        cur.close()
    finally:
        if conn is not None:
            conn.close()

    return organisasi_id





