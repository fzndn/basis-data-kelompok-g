import psycopg2

from simui.settings import DATABASE_CONFIG


def panitia_choice():
    choices = []
    conn = None
    try:
        params = DATABASE_CONFIG
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        cur.execute("""
        SELECT id_kepanitiaan, nama
        FROM kepanitiaan,pembuat_event
        WHERE id_kepanitiaan = id
        ORDER BY id_kepanitiaan
        """)
        row = cur.fetchone()

        while row is not None:
            choices.append((int(row[0]), row[1]))
            row = cur.fetchone()

        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
    return choices


def delete_event(id_event, id_pembuat_event):
    events = []
    conn = None
    try:
        params = DATABASE_CONFIG
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        cur.execute("""
        DELETE FROM event
        WHERE id_event=%s AND id_pembuat_event=%s
        """, (id_event, id_pembuat_event))
        rows_deleted = cur.rowcount
        conn.commit()

        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
    return events


def get_one_event(id_event, id_pembuat_event :str):
    event = None
    conn = None
    try:
        params = DATABASE_CONFIG
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        cur.execute("""
        SELECT nama, tanggal , waktu, kapasitas, harga_tiket, lokasi, sifat_event, deskripsi_singkat, nomor_kategori, jumlah_pendaftar, id_pembuat_event
        FROM event
        WHERE id_event=%s AND id_pembuat_event=%s
        """, (id_event, id_pembuat_event))
        event = cur.fetchone()

        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
    return event

def get_events():
    events = []
    conn = None
    try:
        # SELECT nama, tanggal, waktu, kapasitas, harga_tiket, lokasi, sifat_event, deskripsi_singkat, nomor_kategori, jumlah_pendaftar
        params = DATABASE_CONFIG
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        cur.execute("""
        SELECT E.nama, id_event, id_pembuat_event, P.nama
        FROM event E, pembuat_event P
        WHERE id_pembuat_event = P.id
        """)
        row = cur.fetchone()

        while row is not None:
            events.append(row)
            row = cur.fetchone()

        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
    return events


def org_choices():
    choices = []
    conn = None
    try:
        params = DATABASE_CONFIG
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        cur.execute("""
        SELECT id_organisasi, nama
        FROM organisasi,pembuat_event
        WHERE id_organisasi = id
        ORDER BY id_organisasi
        """)
        row = cur.fetchone()

        while row is not None:
            choices.append((int(row[0]), row[1]))
            row = cur.fetchone()

        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
    return choices


def get_pembuat_event_name(id):
    event = None
    conn = None
    try:
        params = DATABASE_CONFIG
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        cur.execute("""
        SELECT nama
        FROM pembuat_event
        WHERE id=%s
        """, (id,))
        event = cur.fetchone()
        print(event)

        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
    return event


def pembuat_event_choice():
    choices = []
    conn = None
    try:
        params = DATABASE_CONFIG
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        cur.execute("""
        SELECT id, nama
        FROM pembuat_event
        """)
        row = cur.fetchone()

        while row is not None:
            choices.append((row[0], row[1]))
            row = cur.fetchone()

        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
    return choices

def kategori_choice():
    choices = []
    conn = None
    try:
        params = DATABASE_CONFIG
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        cur.execute("SELECT nomor, nama FROM kategori_event ORDER BY nomor")
        row = cur.fetchone()

        while row is not None:
            choices.append((int(row[0]), row[1]))
            row = cur.fetchone()

        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
    return choices


def insert_event(id_pembuat_event,
                 nama,
                 tanggal,
                 waktu,
                 kapasitas,
                 harga_tiket,
                 lokasi,
                 sifat_event,
                 deskripsi_singkat,
                 kategori):
    """ insert a new vendor into the vendors table """
    sql = """
    INSERT INTO event
    (id_event, id_pembuat_event, nama, tanggal, waktu, kapasitas, harga_tiket, lokasi, sifat_event,
    deskripsi_singkat, nomor_kategori)
    VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) RETURNING id_event;
    """
    conn = None
    vendor_id = None
    try:
        # read database configuration
        params = DATABASE_CONFIG
        # connect to the PostgreSQL database
        conn = psycopg2.connect(**params)
        # create a new cursor
        cur = conn.cursor()
        cur.execute('''
        SELECT id_event from event order by id_event desc
        LIMIT 1
        ''')
        id_now = cur.fetchone()[0] + 1
        cur.close()
        cur = conn.cursor()
        cur.execute(sql,
                    (id_now, id_pembuat_event, nama, tanggal, waktu, kapasitas, harga_tiket, lokasi,
                     sifat_event, deskripsi_singkat, kategori))
        event_id = cur.fetchone()[0]

        conn.commit()
        cur.close()
    finally:
        if conn is not None:
            conn.close()

    return vendor_id
