from django import forms

#from event_cud.database import org_choices, panitia_choice, kategori_choice

ORG_CHOICES = [
    (2, 'Feedfire'),
    (42, 'Browsebug'),
]

PANITIA_CHOICES = [
    (43, 'Voonte'),
    (29, 'Feedmix'),
]


class EventForm(forms.Form):
    nama = forms.CharField(
        label='Nama Event',
        max_length=100,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control'
            }
        ))
    organisasi = forms.ChoiceField(
        choices=org_choices(),
        widget=forms.Select(
            attrs={
                'class': 'form-control'
            }
        ),
        label='Organisasi')
    kepanitiaan = forms.ChoiceField(
        choices=panitia_choice(),
        widget=forms.Select(
            attrs={
                'class': 'form-control'

            }
        ),

        label='Kepanitiaan')

    tanggal = forms.DateField(
        widget=forms.DateInput(
            attrs={
                'class': 'form-control',
                'type': 'date'
            }
        ),

        label='Tanggal')
    waktu = forms.TimeField(
        widget=forms.TimeInput(
            attrs={
                'class': 'form-control',
                'type': 'time'
            }
        ),

        label='Waktu')
    kapasitas = forms.IntegerField(
        widget=forms.NumberInput(
            attrs={
                'class': 'form-control'
            }
        ),

        label='Kapasitas')
    harga_tiket = forms.IntegerField(
        widget=forms.NumberInput(
            attrs={
                'class': 'form-control'
            }
        ),

        label='Harga Tiket')
    lokasi = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class': 'form-control'
            }
        ),
        label='Lokasi', max_length=100)
    sifat = forms.ChoiceField(
        choices=[
            ('Umum', 'Umum'),
            ('Private', 'privat'),
        ],
        widget=forms.RadioSelect(
            # attrs={
            #     'class': 'form-control'
            # }
        ),
        label='Sifat Event')
    kategori = forms.ChoiceField(

        choices=kategori_choice(),
        # [
        #     (1,'Seni'),
        #     (3,'hiburan'),
        # ],
        widget=forms.Select(
            attrs={
                'class': 'form-control'
            }
        ),
        label='Kategori Event')
    deskripsi = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class': 'form-control'
            }
        ),
        label='Deskripsi')

    # poster = forms.CharField(
    #     widget=forms.TextInput(
    #         attrs={
    #             'class': 'form-control'
    #         }
    #     ),
    #     label='Poster', max_length=100)
