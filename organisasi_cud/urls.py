from django.conf.urls import url
from django.urls import path

from daftar_organisasi.views import org_detail
from .views import index, form_update, form_cud, organisasi_delete

#url for app
urlpatterns = [
	path('', index, name='organisasi'),
	path('create/', form_cud, name='organisasi-create'),
    path('update/', form_update, name='organisasi-update'),
	path('detail', org_detail, name='organisasi-detail'),
	path('delete/<str:id_organisasi>/', organisasi_delete, name='organisasi-delete'),
]
