from django.conf.urls import url
from django.urls import path

from daftar_kepanitiaan.views import panitia_detail
from .views import kepanitiaan, create_kepanitiaan, update_kepanitiaan

#url for app
urlpatterns = [
	path('', kepanitiaan, name='kepanitiaan'),
	path('create', create_kepanitiaan, name='create-kepanitiaan'),
	path('update', update_kepanitiaan, name='update-kepanitiaan'),
	path('detail', panitia_detail, name='panitia-detail'),
]
