from django.conf.urls import url
from .views import home, login, logout

#url for app
urlpatterns = [
	url(r'^home/', home, name='home'),
    url(r'^login/', login, name='login'),
    url(r'^logout/$', logout, name='logout'),
]
